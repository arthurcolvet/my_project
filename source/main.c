/* this is my new comment*/
#include <stdio.h>
#include "dice.h"

int main(){
	int n;
	printf("How many sides do you want your dice to have?\n");
	scanf("%d",&n);
	initializeSeed();
	printf("Let's roll the dice: %d\n", rollDice(n));
	return 0;
}
